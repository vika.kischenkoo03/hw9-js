// 1.Опишіть, як можна створити новий HTML тег на сторінці.
// Використати document.createElementі або innerHTML

// 2.Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
//elem.insertAdjacentHTML(where, html); 
// where- означає,куди стосовно elem вставляти рядок. Є 4 варіанти:
// beforeBegin- Перед elem.
// afterBegin- Всередину elem, в самий початок.
// beforeEnd- Всередину elem, в кінець.
// afterEnd- Після elem.

// 3.Як можна видалити елемент зі сторінки?
//innerHTML = '',
//remove(),
//removeChild()- видаляє дочірній елемент з DOM 

let array1 = ["hello", "world", "Kyiv", "Kharkiv", "Odessa", "Lviv"];
let array2 = ["1", "2", "3", "sea", "user", 23];
const takeOutList = function (array, parent = document.body){
const list = document.createElement('ul')
array.forEach(element => {
    let item = document.createElement('li')
    item.innerText = element;
    list.appendChild(item);
});
parent.appendChild(list);
}
takeOutList(array1);
takeOutList(array2);